﻿using System;
using System.Reflection;
using Cian.Web.Application;
using Common;
using Microsoft.AspNetCore.Http;
using SimpleInjector;

namespace WebApi
{
    public class App : WebApiStartup
    {
        public static void Main(string[] args) => Run<App>(args);
        
        protected override Assembly Assembly => typeof(App).Assembly;

        protected override void ConfigureDependencyInjectionLibraries(Func<HttpContext> httpContextFactory, Container container, Lifestyle lifestyle)
        {
            base.ConfigureDependencyInjectionLibraries(httpContextFactory, container, lifestyle);
            CommonLibrary.Setup(httpContextFactory, container, lifestyle);
        }
    }
}