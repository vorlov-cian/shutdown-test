﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <summary>
    ///     Тестовый контроллер микросервиса.
    /// </summary>
    public class TestController : Controller
    {
        /// <summary>
        ///     Возвращает значения.
        /// </summary>
        [HttpGet, Route("v1/get-values/")]
        public Task<IEnumerable<string>> GetValuesAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult<IEnumerable<string>>(new[] { "Hello", "world" });
        }
    }
}