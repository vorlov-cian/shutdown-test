﻿using System.Reflection;
using Cian.TestsInfrastructure;
using Cian.TestsInfrastructure.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Rocks.Testing.Moq;
using SimpleInjector;
using WebApi;
using Xunit;

namespace Tests
{
    [Collection("NotParallel")]
    public class WebApiDependencyInjectionConfigTests : DependencyInjectionConfigTestsBase
    {
        [Fact, Trait("Category", "Critical")]
        public override void Setup_Always_Verifiable_AndContainsNoDiagnosticWarnings()
        {
            this.RunTest_Setup_Always_Verifiable_AndContainsNoDiagnosticWarnings();
        }


        [Fact, Trait("Category", "Critical")]
        public override void Setup_Always_DoesNotHaveNonThreadSafeSingletons()
        {
            this.RunTest_Setup_Always_DoesNotHaveNonThreadSafeSingletons();
        }


        [Fact, Trait("Category", "Critical")]
        public override void Setup_Always_DoesNotHaveThreadSafeNonSingletonsServices()
        {
            this.RunTest_Setup_Always_DoesNotHaveThreadSafeNonSingletonsServices();
        }


        protected override void ConfigureSut(Container container, Lifestyle lifestyle)
        {
            var fixture = new FixtureBuilder().WithHttpContext().Build();

            new App().ConfigureDependencyInjectionNoVerify(
                WebTestHelpers.CreateTestApplicationBuilder(container),
                fixture.FreezeMock<IConfiguration>().Object,
                () => fixture.FreezeMock<HttpContext>().Object,
                container,
                lifestyle);
        }


        protected override Assembly GetTestedAssembly() => typeof(App).Assembly;
    }
}