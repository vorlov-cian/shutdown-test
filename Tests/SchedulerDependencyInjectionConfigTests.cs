﻿using System.Reflection;
using Cian.Scheduler;
using Cian.TestsInfrastructure;
using Rocks.ConsoleTasks;
using Scheduler;
using SimpleInjector;
using Xunit;

namespace Tests
{
    [Collection("NotParallel")]
    public class SchedulerDependencyInjectionConfigTests : DependencyInjectionConfigTestsBase
    {
        [Fact, Trait("Category", "Critical")]
        public override void Setup_Always_Verifiable_AndContainsNoDiagnosticWarnings()
        {
            this.RunTest_Setup_Always_Verifiable_AndContainsNoDiagnosticWarnings();
        }


        [Fact, Trait("Category", "Critical")]
        public override void Setup_Always_DoesNotHaveNonThreadSafeSingletons()
        {
            this.RunTest_Setup_Always_DoesNotHaveNonThreadSafeSingletons();
        }


        [Fact, Trait("Category", "Critical")]
        public override void Setup_Always_DoesNotHaveThreadSafeNonSingletonsServices()
        {
            this.RunTest_Setup_Always_DoesNotHaveThreadSafeNonSingletonsServices();
        }


        protected override void ConfigureSut(Container container, Lifestyle lifestyle)
        {
            new Application(new string[0], SchedulerConfigurationDefaults.Create(true)).SetupDependencies(container, lifestyle);
            container.RegisterConsoleTaskServices(lifestyle);
        }


        protected override Assembly GetTestedAssembly()
        {
            return typeof(Application).Assembly;
        }
    }
}