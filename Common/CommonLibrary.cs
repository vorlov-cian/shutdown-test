﻿using System;
using System.Reflection;
using Cian.Core;
using Microsoft.AspNetCore.Http;
using Rocks.Helpers;
using Rocks.SimpleInjector;
using SimpleInjector;

namespace Common
{
    public static class CommonLibrary
    {
        public static void Setup(Func<HttpContext> httpContextFactory,
                                 Container externalContainer = null,
                                 Lifestyle defaultLifeStyle = null)
        {
            var container = externalContainer ?? new Container { Options = { AllowOverridingRegistrations = true } };
            var lifestyle = defaultLifeStyle ?? Lifestyle.Transient;

            if (!LibrariesInitializer.Start(new { Type = typeof(CommonLibrary), container, lifestyle }))
                return;

            RegisterAll(container, lifestyle, httpContextFactory);
        }


        private static void RegisterAll(Container c, Lifestyle lifestyle, Func<HttpContext> httpContextFactory)
        {
            RegisterLibraries(c, lifestyle, httpContextFactory);
            RegisterServices(c, lifestyle, httpContextFactory);
            //RegisterSqlDataAccess(c);
        }


        private static void RegisterLibraries(Container c, Lifestyle lifestyle, Func<HttpContext> httpContextFactory)
        {
            CianCoreLibrary.Setup(httpContextFactory, c, lifestyle);
        }


        private static void RegisterServices(Container c, Lifestyle lifestyle, Func<HttpContext> httpContextFactory)
        {
            var assembly = Assembly.GetExecutingAssembly();

            c.AutoRegisterSelfImplementingTypes(
                assembly,
                lifestyle,
                instancePredicate: type => type.GetConstructors().Length == 1,
                interfacePredicate: type => type.GenericTypeArguments.IsNullOrEmpty(),
                onlyPublicInterfaces: false);

            c.RegisterInstance(httpContextFactory);
        }


//        private static void RegisterSqlDataAccess(Container c)
//        {
//            c.RegisterSingleton<IRealtySqlDataAccessConfiguration>(() => new RealtySqlDataAccessConfiguration("Realty"));
//
//            foreach (var types in RealtySqlDataAccessTypes.Types)
//                c.RegisterSingleton(types.Key, types.Value);
//        }
    }
}