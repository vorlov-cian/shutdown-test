﻿using System.Reflection;
using Cian.Core.Helpers;
using Cian.Scheduler;

namespace Scheduler
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            new Application(args, SchedulerConfigurationDefaults.Create(args, Assembly.GetExecutingAssembly().IsInDebugMode())).Run();
        }
    }
}