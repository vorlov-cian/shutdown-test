using System;
using Cian.Scheduler.Parameters;

namespace Scheduler.Tasks.ProducerTask
{
    internal class ProducerTaskParameters : JsonParameters
    {
        /// <summary>
        ///     Максимальное количество сообщений, которое нужно сгенерировать.
        ///     По умолчанию 10.
        /// </summary>
        public int MaxMessages { get; set; } = 10;
        
        /// <summary>
        ///     Задержка времени между отправкой тестовых сообщений.
        ///     По умолчанию 1 секунда.
        /// </summary>
        public TimeSpan PublishDelay { get; set; } = TimeSpan.FromSeconds(1);
    }
}