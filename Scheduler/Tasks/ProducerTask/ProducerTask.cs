using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Cian.Logging.Helpers;
using Rocks.ConsoleTasks;

namespace Scheduler.Tasks.ProducerTask
{
    /// <summary>
    ///     Таск генерирует тестовые сообщения. 
    /// </summary>
    internal class ProducerTask : ConsoleTask
    {
        
        public ProducerTask()
        {
            
        }

        protected override async Task ProceedInternalAsync(object configuration, CancellationToken cancellationToken = default(CancellationToken))
        {
            await Task.Delay(50000, cancellationToken);
        }
    }
}