﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: InternalsVisibleTo("Rocks.Commands")]
[assembly: InternalsVisibleTo("Tests")]