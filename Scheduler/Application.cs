﻿using System.Collections.Generic;
using System.Reflection;
using Cian.Scheduler;
using Common;
using Rocks.ConsoleTasks;
using Rocks.SimpleInjector;
using SimpleInjector;

namespace Scheduler
{
    internal class Application : SchedulerApplication
    {
        public Application(IEnumerable<string> args, TaskConsoleApplicationConfiguration configuration) : base(args, configuration)
        {
        }


        public override void SetupDependencies(Container c, Lifestyle lifestyle)
        {
            base.SetupDependencies(c, lifestyle);
            CommonLibrary.Setup(() => null, c, lifestyle);
            c.AutoRegisterSelfImplementingTypes(Assembly.GetExecutingAssembly(), lifestyle);
        }
    }
}